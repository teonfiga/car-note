package com.example.carnote.mydb

object MyDbNameClass {
    const val TABLE_NAME = "entry"
    const val TABLE_DETAILS = "details"
    const val TABLE_REPAIRING = "repairing"
    const val ID = "id"
    const val COLUMN_NAME_TITLE = "title"
    const val COLUMN_NAME_COUNT = "count"
    const val COLUMN_NAME_DATE = "date"
    const val COLUMN_NAME_TYPE = "type"

    const val DATABASE_VERSION = 1
    const val DATABASE_NAME = "FeedReader.db"

    const val SQL_CREATE_TABLE =
        "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                "$ID INTEGER PRIMARY KEY," +
                "$COLUMN_NAME_TITLE TEXT," +
                "$COLUMN_NAME_COUNT INTEGER," +
                "$COLUMN_NAME_DATE TEXT," +
                "$COLUMN_NAME_TYPE TEXT)"


    const val SQL_DELETE_TABLE = "DROP TABLE IF EXISTS $TABLE_NAME"

}