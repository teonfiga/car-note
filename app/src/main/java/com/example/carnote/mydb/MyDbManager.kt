package com.example.carnote.mydb

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.carnote.ui.main.data.Items

class MyDbManager(context: Context) {
    private val myDbHelper: MyDbHelper = MyDbHelper(context)
    private var db: SQLiteDatabase? = null

    fun openDb() {
        db = myDbHelper.writableDatabase
    }

    fun insertToDb(title: String, count: Int, date: String, type: String ) {
        val values = ContentValues().apply {
            put(MyDbNameClass.COLUMN_NAME_TITLE, title)
            put(MyDbNameClass.COLUMN_NAME_COUNT, count)
            put(MyDbNameClass.COLUMN_NAME_DATE, date)
            put(MyDbNameClass.COLUMN_NAME_TYPE, type)
        }
        db?.insert(MyDbNameClass.TABLE_NAME, null, values)
    }

    fun readMyDb(): ArrayList<Items> {
        val dataList = ArrayList<Items>()

        val cursor = db?.query(MyDbNameClass.TABLE_NAME, null, null, null, null, null, null)
            while (cursor?.moveToNext()!!) {
                val items = Items(
                    cursor.getString(cursor.getColumnIndex(MyDbNameClass.ID)),
                    cursor.getString(cursor.getColumnIndex(MyDbNameClass.COLUMN_NAME_TITLE)),
                    cursor.getInt(cursor.getColumnIndex(MyDbNameClass.COLUMN_NAME_COUNT)),
                    cursor.getString(cursor.getColumnIndex(MyDbNameClass.COLUMN_NAME_DATE)),
                    cursor.getString(cursor.getColumnIndex(MyDbNameClass.COLUMN_NAME_TYPE)))

                dataList.add(items)
            }
        cursor.close()

        return dataList
    }


    fun deleteData(id : String) : Int? {

        return db?.delete(MyDbNameClass.TABLE_NAME,MyDbNameClass.ID + "= ?", arrayOf(id))
    }

    fun updateData(id: String, title: String, count: Int, date: String, type: String):
            Boolean {
        val values = ContentValues().apply {
            put(MyDbNameClass.ID, id)
            put(MyDbNameClass.COLUMN_NAME_TITLE, title)
            put(MyDbNameClass.COLUMN_NAME_COUNT, count)
            put(MyDbNameClass.COLUMN_NAME_DATE, date)
            put(MyDbNameClass.COLUMN_NAME_TYPE, type)
        }
        db?.update(MyDbNameClass.TABLE_NAME, values, "ID = ?", arrayOf(id))
        return true
    }


    fun closeDb(){
        myDbHelper.close()
    }
}