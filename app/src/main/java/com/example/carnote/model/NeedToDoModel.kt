package com.example.carnote.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.carnote.App
import com.example.carnote.mydb.MyDbManager
import com.example.carnote.ui.main.data.Items


class NeedToDoModel : ViewModel() {

    private val myDbManager: MyDbManager = MyDbManager(App.context).apply {
        openDb()
    }

    val listLiveData:MutableLiveData<List<Items>> = MutableLiveData(myDbManager.readMyDb())


    fun addItem(item: Items){
        myDbManager.insertToDb(item.name, item.count, item.date, item.type)
        val oldList = listLiveData.value ?: arrayListOf()
        val newList = oldList.plus(item)
        listLiveData.value = newList
    }

    override fun onCleared() {
        super.onCleared()
        myDbManager.closeDb()
    }

    fun deleteItem(items: Items) {
        myDbManager.deleteData(items.id)
        val oldList = listLiveData.value ?: arrayListOf()
        val newList = oldList.minus(items)
        listLiveData.value = newList
    }

    fun updateItemDetails(item: Items){
        myDbManager.updateData(item.id, item.name, item.count, item.date, "DetailsSpendNow")
        item.type = "DetailsSpendNow"
        listLiveData.value = listLiveData.value
    }
    fun updateItemRepairing(item: Items){
        myDbManager.updateData(item.id, item.name, item.count, item.date, "RepairingNow")
        item.type = "RepairingNow"
        listLiveData.value = listLiveData.value
    }


}