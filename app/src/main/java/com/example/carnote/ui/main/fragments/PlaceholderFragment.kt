package com.example.carnote.ui.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.example.carnote.R
import com.example.carnote.model.PageViewModel
import com.example.carnote.ui.main.adapters.NeedToDoPagerAdapter
import com.example.carnote.ui.main.fragments.needtodofragments.DetailsSpendToDoFragment
import com.example.carnote.ui.main.fragments.needtodofragments.RepairingToDoFragment
import com.google.android.material.tabs.TabLayout

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment() {

    private val pageViewModel by lazy {
        ViewModelProvider(requireActivity()).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_main, container, false)
        val placeholder = NeedToDoPagerAdapter(childFragmentManager)
        placeholder.addFragment(DetailsSpendToDoFragment(), "DetailsSpend")
        placeholder.addFragment(RepairingToDoFragment(), "Repairing")
        val viewPager: ViewPager = v.findViewById(R.id.view_pager_todo)
        viewPager.adapter = placeholder
        val tabs: TabLayout = v.findViewById(R.id.tablayoutmain)
        tabs.setupWithViewPager(viewPager)
        return v
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}