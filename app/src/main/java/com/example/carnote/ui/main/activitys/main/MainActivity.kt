package com.example.carnote.ui.main.activitys.main

import android.content.IntentSender
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.carnote.R
import com.example.carnote.model.NeedToDoModel
import com.example.carnote.mydb.MyDbManager
import com.example.carnote.ui.main.adapters.SectionsPagerAdapter
import com.example.carnote.ui.main.fragments.NeedToDoFragment
import com.example.carnote.ui.main.fragments.PlaceholderFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import java.util.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        sectionsPagerAdapter.addFragment(PlaceholderFragment(), "NeedToDodoInFuture")
        sectionsPagerAdapter.addFragment(NeedToDoFragment(), "SpendsItems")
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
            tabs.setupWithViewPager(viewPager)
        // val fab: FloatingActionButton = findViewById(R.id.fabAct)

       /* fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }*/
    }
}


