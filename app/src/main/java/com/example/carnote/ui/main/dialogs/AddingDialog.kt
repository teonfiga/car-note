package com.example.carnote.ui.main.dialogs

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.carnote.R
import com.example.carnote.model.NeedToDoModel
import com.example.carnote.ui.main.data.Items
import kotlinx.android.synthetic.main.dialog_adding.*
import java.util.*

class AddingDialog(val type: String) : DialogFragment() {

    private val pageViewModel by lazy {
        ViewModelProvider(requireActivity()).get(NeedToDoModel::class.java)
    }
    private var cal = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_adding, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            cal.set(Calendar.MONTH, month)
            cal.set(Calendar.YEAR, year)
        }

        editDateDialog.setOnClickListener {

        }

        button3.setOnClickListener {
            dismiss()
        }
        button6.setOnClickListener {

            if (TextUtils.isEmpty(editPriceDialog.text.toString())){
                editPriceDialog.error = "Enter price!"
            }
            else {
                pageViewModel.addItem(
                    Items(
                        "1",
                        editTitleDialog.text.toString(),
                        editPriceDialog.text.toString().toInt(),
                        editDateDialog.text.toString(),
                        type
                    )
                )
                dismiss()
            }
        }
    }
    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        editTitleDialog.setText("")
        editPriceDialog.setText("")
        editDateDialog.setText("")
        editPriceDialog.error = null
    }

}