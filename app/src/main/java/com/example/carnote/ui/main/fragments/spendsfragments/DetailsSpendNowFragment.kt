package com.example.carnote.ui.main.fragments.spendsfragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.carnote.R
import com.example.carnote.model.NeedToDoModel
import com.example.carnote.ui.main.adapters.ExpandableAdapter
import com.example.carnote.ui.main.adapters.SecondAdapter
import com.example.carnote.ui.main.dialogs.AddingDialog
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.details_spend_fragment.*
import kotlinx.android.synthetic.main.repairing_to_do_fragment.editText


class DetailsSpendNowFragment: Fragment() {

    private val pageViewModel by lazy {
        ViewModelProvider(requireActivity()).get(NeedToDoModel::class.java)
    }
    private val dialogA = AddingDialog("DetailsSpendNow")
    private val secondAdapter = SecondAdapter(listOf(), {
        pageViewModel.deleteItem(it)
    }, {
        pageViewModel.updateItemDetails(it)

        val tabs: TabLayout = activity!!.findViewById(R.id.tabs)
        tabs.getTabAt(0)!!.select()
    })
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.details_spend_fragment, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_details_spend.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        recycler_details_spend.adapter = secondAdapter
        recycler_details_spend.layoutManager = LinearLayoutManager(requireContext())
        pageViewModel.listLiveData.observe(this, { list ->
            secondAdapter.versionList = list.filter { it.type == "DetailsSpendNow" }
            secondAdapter.notifyDataSetChanged()
            val sumOne = list.filter { it.type == "DetailsSpendNow" }.sumBy { it.count }
            val sum = list.filter { it.type == "DetailsSpendNow" }.sumBy { it.count } +
                    list.filter { it.type == "FuelSpendNow" }.sumBy { it.count } +
                    list.filter { it.type == "RepairingNow" }.sumBy { it.count }

            editText.text = "Spends: Repairing $sumOne / All: $sum "
        })
        fab_details_spend.setOnClickListener {
            dialogA.show(childFragmentManager, "dialogAdding")

        }
        recycler_details_spend.isNestedScrollingEnabled = false
    }

}
