package com.example.carnote.ui.main.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class NeedToDoPagerAdapter(fm: FragmentManager)
    : FragmentPagerAdapter(fm) {

    private val fragments: ArrayList<Fragment> = ArrayList<Fragment>()
    private val titles: ArrayList<String> = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).


        return fragments[position]
        //   return PlaceholderFragment.newInstance(position + 1)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        // return context.resources.getString(TAB_TITLES[position])
        return titles[position]
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return fragments.size
    }

    fun addFragment(fragment: Fragment, title: String){
        fragments.add(fragment)
        titles.add(title)
    }
}