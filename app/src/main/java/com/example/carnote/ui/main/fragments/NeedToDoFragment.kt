package com.example.carnote.ui.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.carnote.R
import com.example.carnote.ui.main.adapters.NeedToDoPagerAdapter
import com.example.carnote.ui.main.fragments.spendsfragments.DetailsSpendNowFragment
import com.example.carnote.ui.main.fragments.spendsfragments.FuelSpendNowFragment
import com.example.carnote.ui.main.fragments.spendsfragments.RepairingNowFragment
import com.google.android.material.tabs.TabLayout


class NeedToDoFragment() : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_need_to_do, container, false)
        // Inflate the layout for this fragment
        val sectionsPagerAdapter = NeedToDoPagerAdapter(childFragmentManager)

        sectionsPagerAdapter.addFragment(FuelSpendNowFragment(), "FuelSpend")
        sectionsPagerAdapter.addFragment(DetailsSpendNowFragment(), "DetailsSpend")
        sectionsPagerAdapter.addFragment(RepairingNowFragment(), "Repairing")
        val viewPager: ViewPager = v.findViewById(R.id.view_pager_todo)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = v.findViewById(R.id.tablayouttodo)
        tabs.setupWithViewPager(viewPager)

        return v
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): NeedToDoFragment {
            return NeedToDoFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}