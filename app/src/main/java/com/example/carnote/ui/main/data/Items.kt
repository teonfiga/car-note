package com.example.carnote.ui.main.data

data class Items(val id: String,
    val name: String, val count: Int, val date: String,
    var type: String, var expandable: Boolean = false)


