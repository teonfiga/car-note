package com.example.carnote.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.carnote.R
import com.example.carnote.ui.main.data.Items

class SecondAdapter(
    var versionList: List<Items>,
    val deleteCallback:(Items)->Unit,
    val updateCallback:(Items)->Unit
) : RecyclerView.Adapter<SecondHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SecondHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.items_did_recycler, parent, false)

        return SecondHolder(view)
    }

    override fun onBindViewHolder(holder: SecondHolder, position: Int) {

        val versions : Items = versionList[position]

        holder.Cout.text = versions.name + "/" + versions.count.toString() + "UAH/" + versions.date

        holder.del.setOnClickListener {
            deleteCallback.invoke(versions)
        }

    }

    override fun getItemCount(): Int {
        return versionList.size
    }
}

class SecondHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val Cout: TextView = itemView.findViewById(R.id.textView1)
    val del: View = itemView.findViewById(R.id.button41)
    val edit: View = itemView.findViewById(R.id.button51)

}