package com.example.carnote.ui.main.fragments.spendsfragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.carnote.R
import com.example.carnote.model.NeedToDoModel
import com.example.carnote.ui.main.adapters.ExpandableAdapter
import com.example.carnote.ui.main.adapters.SecondAdapter
import com.example.carnote.ui.main.data.Items
import com.example.carnote.ui.main.dialogs.AddingDialog
import kotlinx.android.synthetic.main.details_spend_fragment.*
import kotlinx.android.synthetic.main.repairing_now_fragment.*
import kotlinx.android.synthetic.main.repairing_to_do_fragment.editText

class RepairingNowFragment : Fragment() {

    private val pageViewModel by lazy {
        ViewModelProvider(requireActivity()).get(NeedToDoModel::class.java)
    }
    private val dialogA = AddingDialog("RepairingNow")

    val expandableAdap = SecondAdapter(listOf(), {
        pageViewModel.deleteItem(it)
    }, {
        pageViewModel.updateItemRepairing(it)
    })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.repairing_now_fragment, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_repairing_now.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        recycler_repairing_now.adapter = expandableAdap
        recycler_repairing_now.layoutManager = LinearLayoutManager(requireContext())
        pageViewModel.listLiveData.observe(this, { list ->
            expandableAdap.versionList = list.filter { it.type == "RepairingNow" }
            expandableAdap.notifyDataSetChanged()
            val sumOne = list.filter { it.type == "RepairingNow" }.sumBy { it.count }
            val sum = list.filter { it.type == "DetailsSpendNow" }.sumBy { it.count } +
                    list.filter { it.type == "FuelSpendNow" }.sumBy { it.count } +
                    list.filter { it.type == "RepairingNow" }.sumBy { it.count }
            editText.text = "Spends: Repairing $sumOne / All: $sum "
        })
        fab_repairing_now.setOnClickListener {
            dialogA.show(childFragmentManager, "dialogAdding")
        }
        recycler_repairing_now.isNestedScrollingEnabled = false
    }

}