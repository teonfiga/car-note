package com.example.carnote.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.carnote.R
import com.example.carnote.ui.main.data.Items

class ExpandableAdapter(
    var versionList: List<Items>,
    val deleteCallback:(Items)->Unit,
    val updateCallback:(Items)->Unit
    ) : RecyclerView.Adapter<ExpandableHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpandableHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.items_recyclerview, parent, false)

        return ExpandableHolder(view)
    }

    override fun onBindViewHolder(holder: ExpandableHolder, position: Int) {

        val versions : Items = versionList[position]

        holder.Cout.text = versions.name + "/" + versions.count.toString() + "UAH/" + versions.date

        holder.del.setOnClickListener {
            deleteCallback.invoke(versions)
        }
        holder.update.setOnClickListener {
            updateCallback.invoke(versions)
        }

    }

    override fun getItemCount(): Int {
        return versionList.size
    }
}

class ExpandableHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val Cout: TextView = itemView.findViewById(R.id.textView)
    val del: View = itemView.findViewById(R.id.button4)
    val update: View = itemView.findViewById(R.id.button8)
    val edit: View = itemView.findViewById(R.id.button5)

}

