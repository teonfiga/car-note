package com.example.carnote

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.raizlabs.android.dbflow.config.FlowManager

class App : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
    }
    override fun onCreate() {
        super.onCreate()
        FlowManager.init(this)
        context = this.applicationContext
    }

    override fun onTerminate() {
        super.onTerminate()
        FlowManager.destroy()
    }
}